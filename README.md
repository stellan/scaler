# Scaler Microservice

Example service using imagemagick in a Docker image to encapsulate dependencies. The service has a single endpoint which accepts dimensions and URL arguments, downloads the URL content, resizes it to the supplied dimensions and returns the scaled image applying the default mime type based on the extension. 

## Prerequisites
In order to run the application, Docker needs to be installed.

## Build it
To build the Docker image:

`docker build -t scaler-service:1.0 .`

## Run it
To start a container based on the previously created image:

`docker run --rm -p 3000:3000 --name scaler-service scaler-service:1.0`

## Try it
In order to test the application, point your browser to the following URL:

`http://localhost:3000/resize/320x240/http:%2F%2Fworldartsme.com%2Fimages%2Fofficial-soccer-clipart-1.jpg``

The last parameter could be replaced with another URL containing another image.

