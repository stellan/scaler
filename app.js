/*
 * This is a simple sample service that wraps the imagemagick 'convert' command
 * behind a simple HTTP get interface
 * 
 * The example is all but production grade. To make it usable in a production
 * environment, at least the following would be necessary:
 * 
 *  - Input validation of the :dimension and :url parameters. The current setup
 *    that simply passes the arguments to an executable is easily hacked and
 *    opens up the ability to execute arbitrary code on the host
 * 
 *  - Error handling is non existing
 * 
 *  - API documentation using swagger or similar is missing.
 * 
 *  - Original and converted files are kept in the application directory and
 *    should be cleaned up
 * 
 */
const http = require('http');
const fs = require('fs');
const url = require("url");
const path = require("path");
const exec = require('child_process').exec;
const express = require('express');
const app = express();

/*
 * Route for resizing an image dynamically based on the width and height passed
 * in the :dimension parameter. The format is WxH where W and H are integer 
 * values, e.g. 320x240. The resizing is performed using the default imagemagick
 * behavior, i.e. maintaining the aspect ratio.
 */
app.get('/resize/:dimensions/:url', (req, res) => {
  var dimensions = req.params.dimensions;
  var parsed = url.parse(req.params.url);
  var filename = path.basename(parsed.pathname).replace("/", "|");
  var infile = '/tmp/' + filename;
  var outfile = '/tmp/_' + dimensions + '_' + filename;
  download(req.params.url, infile)
    .then(() => console.log("Saved file"))
    .then(() => execp('convert ' + infile + ' -resize ' + dimensions + ' ' + outfile))
    .then(() => res.sendFile(outfile))
    .catch((err) => { console.log(err); res.send(err) });
});


/*
 * A simple promise wrapper for the exec funtionality. Since the
 * callback is processed once the subprocess exits, the promise
 * will not resolve until the process has exited.
 */
var execp = function (cmd, env) {
  return new Promise((resolve, reject) => {
    exec(cmd, env, (err, result) => {
      if (err) { reject(err) }
      else { resolve(result) }
    })
  })
}

/*
 * A simple promise wrapper for the download funtionality. 
 */
var download = function (url, dest) {
  return new Promise((resolve, reject) => {
    _download(url, dest, (err, result) => {
      if (err) {
        reject(err);
      }
      else {
        resolve(result);
      }
    });
  });
}

/*
 * Downloads the content of the passed URL and stores it
 * in the specified destination file.  
 */
var _download = function (url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = http.get(url, function (response) {
    response.pipe(file);
    file.on('finish', function () {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function (err) { 
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};

// Start the express HTTP listener
app.listen(3000, () => console.log('Started on port 3000'))

