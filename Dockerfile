FROM node:9-alpine

# Install dependencies
RUN apk add --no-cache imagemagick

# Copy the application
ADD . /opt/app/

# Install node modules
WORKDIR /opt/app
RUN npm install

EXPOSE 3000
ENTRYPOINT node /opt/app/app.js

